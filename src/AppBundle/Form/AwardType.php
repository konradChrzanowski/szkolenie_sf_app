<?php

namespace AppBundle\Form;

use AppBundle\Entity\Award;
use AppBundle\Entity\Project;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AwardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Tytuł'
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Opis',
            ))
            ->add('type', ChoiceType::class, array(
                'label' => 'Typ',
                'choices' => [
                    'Pierwsze miejsce' => 'pierwsze_miejsce',
                    'Wyróznienie' => 'wyroznienie',
                ],

            ))
            ->add('project', EntityType::class, array(
                'label' => 'Projekt',
                'class' => 'AppBundle\Entity\Project',
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Award::class,
        ));
    }

}