<?php

namespace AppBundle\Form;

use AppBundle\Entity\Project;
use AppBundle\Form\Type\AddressType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Nazwa'
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Opis'
            ))
            ->add('isPublished', CheckboxType::class, array(
                'label' => 'Publikowany',
            ))
            ->add('image', FileType::class, array(
               'label' => 'Obraz',
            ));
//            ->add('awards', CollectionType::class, [
//                'entry_type' => AwardType::class,
//                'allow_add' => true,
//                'allow_delete' => true,
//                'prototype' => true,
//                'required' => false,
//                'attr' => [
//                    'class' => 'awards-collection',
//                ],
//            ])
//            ->add('adress',
//                AddressType::class, ['mapped' => false, 'label' => 'Adres']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Project::class,
        ));
    }

}