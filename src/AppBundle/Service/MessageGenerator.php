<?php

namespace AppBundle\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class MessageGenerator
{
    private $authorizationChecker;
    private $logger;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker, LoggerInterface $logger)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->logger = $logger;
    }

    /**
     * @param $string
     * @return string
     */
    public function generateMessage($string)
    {
        if (!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }

        $message = uniqid($string);
        $this->logger->info($message);

        return $message;
    }
}