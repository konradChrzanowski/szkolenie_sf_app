<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Project;
use AppBundle\Service\MessageGenerator;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\Cache\Simple\RedisCache;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage", )
     */
    public function indexAction(MessageGenerator $messageGenerator)
    {
//        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Nie masz dostępu!');

//        $messageGenerator->generateMessage('test');

        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
//            throw new AccessDeniedException();
        }

//        $user = $this->getUser();

        return $this->render('default/index.html.twig');
    }

    /**
     * Listing projektów na stronie głównej
     * @Route("/projects", name="projects_index")
     *
     */
    public function projectsIndexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('AppBundle:Project')->findQueryForPagination();
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            3/*limit per page*/
        );

        $sorted = $em->getRepository('AppBundle:Project')
            ->findPublishedAndSortedByDate();


//        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY', null, 'Nie jesteś zalogowany');
//        $redisClient = RedisAdapter::createConnection('redis://localhost');
//        $redisService = $this->container->get('snc_redis.default');
//        $redisService->set('klucz', 'wartosc');
//        dump($redisService->get('klucz'));

        // Cache component PSR-6

//        $cache = $this->get('cache.app');
//        $projectsCountItem = $cache->getItem('projects.count');
//
//        if (!$projectsCountItem->isHit()) {
//            $cached = 'not cached';
//            $projectsCountItem->set(count($projects));
//            $cache->save($projectsCountItem);
//        } else {
//            $cached = 'cached';
//        }
//
//        $count = $projectsCountItem->get();
        // End cache PSR-6

        //paginacja



        $response = $this->render('default/projects.html.twig', [
            'pagination' => $pagination,
        ]);

//        $response->setEtag(md5($response->getContent()));
//        $response->setPublic();
//        $response->isNotModified($request);

        return $response;
    }

    /**
     * Szczegóły projektu
     * @Route("/project/{slug}", name="project_show")
     */
    public function projectShowAction(Project $project, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $comments = $em->getRepository('AppBundle:Comment')->findBy(array(
            'project' => $project,
        ));

        $response = $this->render('default/project.html.twig', array(
            'project' => $project,
            'comments' => $comments,
        ));



        return $response;
    }
}
