<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Award;
use AppBundle\Entity\Project;
use AppBundle\Form\AwardType;
use AppBundle\Form\ProjectType;
use EasySlugger\Utf8Slugger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/awards", name="admin_award_")
 */
class AwardController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function awardListAction()
    {
        $awards = $this->getDoctrine()->getManager()->getRepository('AppBundle:Award')->findAll();

        return $this->render('admin/award/index.html.twig', [
            'awards' => $awards,
        ]);
    }

    /**
     * @Route("/add_noform")
     */
    public function addAction()
    {
        $award = new Award();

        $award->setTitle('aa');
        $award->setDescription('Lodosapdasdpoaspdoasod');
        $award->setType('aaa');

        $validator = $this->get('validator');

        $errors = $validator->validate($award);

        if (count($errors) > 0) {
            return new Response((string)$errors);
        }

        return new Response('OK');
    }

    /**
     * Dodawanie nagrody
     * @Route("/add", name="add")
     */
    public function awardAddAction(Request $request)
    {
        $award = new Award();

        $form = $this->createForm(AwardType::class, $award);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($award);
            $em->flush();

            return $this->redirectToRoute('admin_award_index');
        }

        return $this->render('admin/award/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Szczegóły nagrody
     * @Route("/project/award/{id}", name="show")
     */
    public function awardShowAction(Award $award)
    {
        return $this->render('admin/award/show.html.twig', array(
            'award' => $award,
        ));
    }

    /**
     * Edycja nagrody
     * @Route("/edit/{id}", name="edit")
     */
    public function awardEditAction(Award $award, Request $request)
    {
        $form = $this->createForm(AwardType::class, $award);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($award);
            $em->flush();

            $this->addFlash('success' ,'Zapisano zmiany');
        }

        return $this->render('admin/award/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function awardRemoveAction(Award $award)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($award);

        $this->addFlash('success', 'Usunięto');

        return $this->redirectToRoute('admin_award_index');
    }
}
