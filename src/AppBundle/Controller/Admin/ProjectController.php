<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Award;
use AppBundle\Entity\Project;
use AppBundle\Form\AwardType;
use AppBundle\Form\ProjectType;
use EasySlugger\Utf8Slugger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/projects", name="admin_project_")
 */
class ProjectController extends Controller
{
    /**
     * Lista projektów
     * @Route("/", name="list")
     */
    public function projectListAction()
    {
        $em = $this->getDoctrine()->getManager();
        $projects = $em->getRepository('AppBundle:Project')->findAll();

        return $this->render('admin/project/index.html.twig', array(
            'projects' => $projects,
        ));
    }

    /**
     * Dodawanie projektu
     * @Route("/add", name="add")
     */
    public function projectAddAction(Request $request)
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();


            //todo lifecyclecallbacks
            $slugger = new Utf8Slugger();
            $slug = $slugger::slugify($project->getName());
            $project->setSlug($slug);

            $savePath = $this->getParameter('project_images_directory');

            //Upload pliku
            /** @var UploadedFile $imageFile */
            $imageFile = $project->getImage();
            $imageFilename = $project->getSlug().'.'.$imageFile->guessExtension();

            $imageFile->move(
                $savePath,
                $imageFilename
            );

            //mozemy użyć też Service/FileUploader
            $project->setImage($imageFilename);

            $em->persist($project);
            $em->flush();

            $this->addFlash('success', 'Dodano projekt');

            return $this->redirectToRoute('admin_index');
        }

        return $this->render('admin/project/add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Szczegóły projektu
     * @Route("/project/show/{id}", name="show")
     */
    public function projectShowAction(Project $project)
    {
        return $this->render('admin/project/show.html.twig', array(
            'project' => $project,
        ));
    }

    /**
     * Edycja projektu
     * @Route("/project/edit/{id}", name="edit")
     */
    public function projectEditAction(Project $project, Request $request)
    {
        $form = $this->createForm(ProjectType::class, $project);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $slugger = new Utf8Slugger();
            $slug = $slugger::slugify($project->getName());
            $project->setSlug($slug);

            $em->flush();

            $this->addFlash('success', 'Zapisano zmiany');
        }

        return $this->render('admin/project/edit.html.twig', array(
            'form' => $form->createView(),
            'project' => $project,
        ));
    }

    /**
     * Usuwanie projektu
     * @Route("/project/remove/{id}", name="remove")
     */
    public function projectRemoveAction(Project $project)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($project);
        $em->flush();

        $this->addFlash('success', 'Usunięto');

        return $this->redirectToRoute('projects_index');
    }

    /**
     * @Route("/project_categories", name="categories_list")
     */
    public function projectCategoryListAction()
    {
        $projectCategories = $this->getDoctrine()->getManager()->getRepository('AppBundle:ProjectCategory')->findAll();

        return $this->render('admin/project_category/index.html.twig', array(
            'project_categories' => $projectCategories,
        ));
    }
}
