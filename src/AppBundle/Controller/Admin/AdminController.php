<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Project;
use AppBundle\Form\ProjectType;
use EasySlugger\Utf8Slugger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin", name="admin_")
 */
class AdminController extends Controller
{
    /**
     * Strona główna panelu administracyjnego
     * @Route("/", name="index")
     */
    public function adminIndexAction(Request $request)
    {
        return $this->render('admin/index.html.twig');
    }
}
