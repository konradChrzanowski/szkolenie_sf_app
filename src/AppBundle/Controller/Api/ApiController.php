<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Project;
use Doctrine\Common\Annotations\AnnotationReader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/api", name="api_")
 */
class ApiController extends Controller
{
    /**
     * @Route("/projects", name="projects")
     * @Method("POST")
     */
    public function projectsAction()
    {
        $data = [
            'foo' => 'bar',
            'lorem' => 'ipsum',
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/project/{id}")
     * @Method("GET")
     */
    public function projectGetAction(Project $project)
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        //Serializer
        $encoders = [new JsonEncoder(), new XmlEncoder(), new CsvEncoder()];
        $normalizers = [new ObjectNormalizer($classMetadataFactory)];

        $serializer = new Serializer($normalizers, $encoders);

        $data = $serializer->normalize($project, null, ['groups' => ['group1', 'group2']]);

        dump($data);

        return new Response('<html><head></head><body></body></html>');

        $jsonContent = $serializer->serialize($project, 'json');
        $xmlContent = $serializer->serialize($project, 'xml');
        $csvContent = $serializer->serialize($project, 'csv');


        return new Response($csvContent);
        return new JsonResponse($jsonContent);
    }

    /**
     * @Route("/project/{id}")
     * @Method("PUT")
     */
    public function projectPutAction(Project $project)
    {
        return new JsonResponse();
    }
}