<?php

namespace AppBundle\Controller\Project;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Project;
use AppBundle\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CommentController extends Controller
{
    /**
     * @Route("/comment/{id}/edit", name="project_comment_edit")
     */
    public function commentEditAction(Comment $comment, Request $request)
    {
        $this->denyAccessUnlessGranted('edit', $comment);

        $form = $this->createForm(CommentType::class, $comment);


        //ACL
//        $authorizationChecker = $this->get('security.authorization_checker');

//        if (!$authorizationChecker->isGranted('EDIT', $comment)) {
//            throw new AccessDeniedException();
//        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('projects_index');
        }

        return $this->render('comment/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/project/{id}/comment/add", name="project_comment_add")
     */
    public function commentAddAction(Project $project, Request $request)
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $comment->setProject($project);
            $comment->setOwner($this->getUser());
            $entityManager->persist($comment);
            $entityManager->flush();

            // creating the ACL
            /*$aclProvider = $this->get('security.acl.provider');
            $objectIdentity = ObjectIdentity::fromDomainObject($comment);
            $acl = $aclProvider->createAcl($objectIdentity);

            // retrieving the security identity of the currently logged-in user
            $tokenStorage = $this->get('security.token_storage');
            $user = $tokenStorage->getToken()->getUser();
            $securityIdentity = UserSecurityIdentity::fromAccount($user);

            // grant owner access
            $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OWNER);
            $aclProvider->updateAcl($acl);*/

            return $this->redirectToRoute('project_show', [
                'slug' => $project->getSlug(),
            ]);
        }

        return $this->render('comment/add.html.twig', [
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/comment/{id}/remove", name="project_comment_remove")
     */
    public function commentRemoveAction(Comment $comment)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();

        return $this->redirectToRoute('project_show', [
            'slug' => $comment->getProject()->getSlug(),
        ]);
    }
}
