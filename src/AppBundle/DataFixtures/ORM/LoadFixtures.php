<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Project;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;

class LoadFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
//        $project = new Project();
//        $project->setTitle('Octopus'.rand(1, 100));
//
//        $project->setStatus(Project::STATUS_DONE);
//        $project->setDescription('Lorem Ipsum');
//
//        $manager->persist($project);
//        $manager->flush();

        $objects = Fixtures::load(
            __DIR__.'/fixtures.yml',
            $manager,
            [
                'providers' => [$this]
            ]
        );
    }
}