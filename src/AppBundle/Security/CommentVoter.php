<?php

namespace AppBundle\Security;

use AppBundle\Entity\Comment;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CommentVoter extends Voter
{
    // these strings are just invented: you can use anything
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        $supported = [
            self::VIEW,
            self::EDIT,
            self::DELETE
        ];
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, $supported)) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof Comment) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        // you know $subject is a Comment object, thanks to supports
        /** @var Comment $comment */
        $comment = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($comment, $user);
            case self::EDIT:
                return $this->canEdit($comment, $user);
            case self::DELETE:
                return $this->canDelete($comment, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Comment $comment, User $user)
    {
        // if they can edit, they can view
        if ($this->canEdit($comment, $user)) {
            return true;
        }

        // the Comment object could have, for example, a method isPrivate()
        // that checks a boolean $private property
        return true;
    }

    private function canEdit(Comment $comment, User $user)
    {
        return $user === $comment->getOwner();
    }

    private function canDelete(Comment $comment, User $user)
    {
        return $this->canEdit($comment, $user);
    }
}