<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Award;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Monolog\Logger;

class EmailNotifier
{
    private $mailer;
    private $logger;

    public function __construct(\Swift_Mailer $mailer, Logger $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $object = $args->getEntity();

        if (!($object instanceof Award)) {
            return;
        }

        $objectManager = $args->getEntityManager();

        $project = $object->getProject();
        $project->setName('Nowa konfiguracja');
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $object = $args->getEntity();

        if (!($object instanceof Award)) {
            return;
        }

        $objectManager = $args->getEntityManager();

//        $this->mailer->send($message);
        $this->logger->addInfo('Post persist log');
    }
}