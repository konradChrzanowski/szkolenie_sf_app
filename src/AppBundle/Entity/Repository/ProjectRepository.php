<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class ProjectRepository extends EntityRepository
{
    public function findQueryForPagination()
    {
        $query = $this->createQueryBuilder('p')
            ->select('p')
            ->andWhere('p.isPublished = :is_published')
            ->setParameter('is_published', true)
            ->addOrderBy('p.createdAt', 'DESC')
            ->getQuery();

        return $query;
    }

    public function findPublishedAndSortedByDate()
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->andWhere('p.isPublished = :is_published')
            ->setParameter('is_published', true)
            ->addOrderBy('p.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

    }
}