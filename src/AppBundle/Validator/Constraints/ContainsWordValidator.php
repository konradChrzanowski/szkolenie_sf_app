<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsWordValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (strpos($value, $constraint->word) === false) {
            $this->context
                ->buildViolation($constraint->message)
                ->setParameter('{{ word }}', $constraint->word)
                ->addViolation();
        }
    }
}