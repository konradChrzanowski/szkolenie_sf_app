<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsWord extends Constraint
{
    public $message = 'Musi zawierać słowo "{{word}}"';

    public $word;

    public function __construct($options = null)
    {
        parent::__construct($options);
    }
}